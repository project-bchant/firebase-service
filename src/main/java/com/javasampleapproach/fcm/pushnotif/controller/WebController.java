package com.javasampleapproach.fcm.pushnotif.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.fcm.pushnotif.service.AndroidPushNotificationsService;

@RestController
public class WebController {

	//private final String TOPIC = "JavaSampleApproach";
	//private final String to = "d85CnInnIH8:APA91bFL-nepCdsf6iY82gB1AD3Yyvicyf9jfedP-YsHopqjFaJo3qqzkLxV3WOrD748PtNWMhUZbrPUymkmkSG6fgLiPEioTH4RIFoRy7aTgQtPoT0InOoycHXgzQL8cs5NZPnRo-f8";
	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@RequestMapping(value = "/send", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> send(@RequestHeader("To") String to, @RequestHeader("TrxID") String trxId) throws JSONException {

		JSONObject body = new JSONObject();
		body.put("to", to);
		body.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", "Pembayaran Berhasil");
		notification.put("body", "Pembayaran ini sudah lunas");
		
		JSONObject data = new JSONObject();
		data.put("desc", "success");
		data.put("trxId", trxId);

		body.put("notification", notification);
		body.put("data", data);

		HttpEntity<String> request = new HttpEntity<>(body.toString());

		CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
		CompletableFuture.allOf(pushNotification).join();

		try {
			String firebaseResponse = pushNotification.get();
			
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/sendFailed", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> sendFailed(@RequestHeader("To") String to, @RequestHeader("TrxID") String trxId) throws JSONException {
	
		JSONObject body = new JSONObject();
		body.put("to", to);
		body.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", "Pembayaran Gagal");
		notification.put("body", "");
		
		JSONObject data = new JSONObject();
		data.put("desc", "error");
		data.put("trxId", trxId);

		body.put("notification", notification);
		body.put("data", data);

		HttpEntity<String> request = new HttpEntity<>(body.toString());

		CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
		CompletableFuture.allOf(pushNotification).join();

		try {
			String firebaseResponse = pushNotification.get();
			
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}
}
